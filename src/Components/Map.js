import React from 'react';
import { GoogleMap, Marker, withScriptjs, withGoogleMap, Polygon } from 'react-google-maps';
import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";

const polyClick = (state) => {
  console.log(state.paths);
}

const markerClick = (marker) => {
  console.log(marker['Tracking Name']);
}


const Map = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={props.zoom}
    defaultCenter={{ lat: props.center.lat, lng: props.center.lng }}
  >
    {/* {props.states.map((state, index) => {
      return <Polygon key={index} paths={state.paths} onClick={() => polyClick(state)} />
    })} */}
    <MarkerClusterer
      averageCenter
      gridSize={60}
    >
    {props.isMarkerShown && props.markers.map((marker, index) => {
      return <Marker key={index} position={{ lat: marker.latitude, lng: marker.longitude }} onClick={() => markerClick(marker)} />
    })}
    </MarkerClusterer>
  </GoogleMap>
))

export default Map;
