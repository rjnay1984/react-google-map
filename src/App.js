import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import styled from 'styled-components';
import DATA from './data/test';
import STATES from './data/states';

import Map from './Components/Map';
import MarkerDetail from './Components/MarkerDetail';

const FixedButton = styled(Button)`
  position: fixed;
  bottom: 1em;
  left: 1em;
  z-index: 9999;
`;

class App extends Component {
  state = {
    data: DATA,
    states: STATES,
    center: {
      lat: 39.8283459,
      lng: -98.5794797
    },
    zoom: 5.5,
  }
  render() {
    return (
      <Router>
        <div className="App">
          <Route
            exact
            path="/"
            render={() => (
              <Map
                isMarkerShown
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAajSdtjMR_UvNkn31N_s5xUG8tDFooS0w"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `100vh` }} />}
                mapElement={<div style={{ height: `100%` }} />}
                markers={this.state.data}
                center={this.state.center}
                zoom={this.state.zoom}
                states={this.state.states}
              />
            )}
          />
          <Route
            path="/marker/:id"
            render={() => <MarkerDetail />} 
          />
          <FixedButton tag={Link} to="/">Home</FixedButton>
        </div>
      </Router>
    );
  }
}

export default App;
